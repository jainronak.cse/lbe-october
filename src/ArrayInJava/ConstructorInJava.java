package ArrayInJava;

public class ConstructorInJava {
 
	int b;
	
	public ConstructorInJava() {
		System.out.println("in cons");
		b=55;
	}
	public ConstructorInJava(int a) {
		System.out.println("in param cons");
		b=a;
	}
	
	public void init(int a) {
		b=a;
	}
	public void show() {
		System.out.println(b);
	}
	
	
	public static void main(String[] args) {
	
		ConstructorInJava obj = new ConstructorInJava(10);
//		obj.init(45);
		obj.show();
	}
}
