package ArrayInJava;

public class InstanceAndLocalVariable {

	int a =20;  // instance variable, Global

	public void show() {
		a=30;
		System.out.println(a);
		int a = 90;    // local variable
		System.out.println(a);
		a=98;
	}

	public void display() {
		show();
		
		System.out.println(a);
		int a = 88;  
	}
	
	public void sum(int a) {
		System.out.println(a);
	}
	public static void main(String[] args) {

		InstanceAndLocalVariable obj = new InstanceAndLocalVariable();
//		obj.show();
//		obj.display();
//		obj.sum(23);
		System.out.println(obj.a);
	}
}
