package ArrayInJava;

public class MethodsInJava {

	public int sum(int a, int b) {
		int c = a+b;
		return c;
	}
	public int sub(int a, int b) {
		int c = a-b;
		return c;
	}
	public int mul(int a, int b) {
		int c = a*b;
		return c;
	}
	public float div(int a, int b) {
		int c = a/b;
		return c;
	}
	
	
	public static void main(String[] args) {
		MethodsInJava obj = new MethodsInJava();
		int i =obj.sum(10, 20);
		System.out.println(i);
		
		float p = obj.div(43, 10);
		System.out.println(p);
		
	}
	
	
}
