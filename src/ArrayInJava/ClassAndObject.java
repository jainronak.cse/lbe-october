package ArrayInJava;

public class ClassAndObject {

	public ClassAndObject() {
		
	}
	public void show() {
		
		System.out.println("in show");
	}

	public void print(int a, String str) {
		System.out.println("in print");
		
	}
	
	public void evenOrOdd(int i) {
		if(i%2==0) {
			System.out.println("it is even");
		}else {
			System.out.println("it is odd");
		}
	}
	
	public boolean eOrO(int i) {
		if(i%2==0) {
			System.out.println("even");
			return true;
		}else {
			System.out.println("odd");
			return false;
		}
	}
	
	public void printDay(boolean by) {
		if(by==true) {
			System.out.println("Employee days are MWF");
		}else {
			System.out.println("Employee days are TTS");
		}
	}
	public void printset(boolean by) {
		if(by==true) {
			System.out.println("Set a");
		}else {
			System.out.println("Set b");
		}
	}
	private int returnDay() {
		return 1;
	}

	public static void main(String[] args) {

		ClassAndObject obj = new ClassAndObject();
		obj.evenOrOdd(45);
		boolean b = obj.eOrO(10);
		obj.printDay(b);
		
		boolean bu = obj.eOrO(13);
		obj.printset(bu);
		
		int i = obj.returnDay();
		System.out.println(i);
	}
}
