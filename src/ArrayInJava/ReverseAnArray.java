package ArrayInJava;

public class ReverseAnArray {

	public static void main(String[] args) {
		
		int arr[] = {2,3,4,6,7};
		int temp;
		int len = arr.length;
		for(int j=0;j<len/2;j++) {			
			temp = arr[j];
			arr[j] = arr[len-1-j];
			arr[len-1-j] = temp;
		}
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("arr["+i+"] is : " + arr[i]);
		}	
		
	}
}
















