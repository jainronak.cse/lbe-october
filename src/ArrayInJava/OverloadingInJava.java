package ArrayInJava;

public class OverloadingInJava {

	public void sum(int a, int b) {
		System.out.println(a + b);
	}
	public int sum(int a, long b) {
		System.out.println(a + b);
		return 0;
	}
	
	public void sum(float a, int b) {
		System.out.println(a + b);
	}

	public void sum(int a, int b, int c) {
		System.out.println(a + b + c);
	}

	public static void main(String[] args) {
		
		OverloadingInJava obj  = new OverloadingInJava();
		obj.sum(12.2f, 3);
		
	}
}
