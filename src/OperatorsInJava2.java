
public class OperatorsInJava2 {

	public static void main(String[] args) {

//		Ternary operator

//		int a = 35;
//		int b = (a % 2 == 0) ? 1 : 0;
//		System.out.println(b);

//		unary operator
//		exp++,exp--   > postfix
//		++exp , --exp > prefix

		int i = 2;
		int temp;

//		temp = i++;    // temp = 2;
//		temp = ++i;    // temp = 3;
//		temp = i++ + i++;  // temp  = 2 + 3;
//		temp = ++i + i++;  // temp = 3 + 3 ;
//		temp = --i + --i + ++i + i++ ;  // temp = 1 + 0 +1 +1
//		temp = i-- - i-- + ++i + ++i ; // temp = 2 - 1 + 1 + 2
//		temp = i-- + --i + i++ + ++i; // temp = 2 + 0 + 0 + 2
//		temp = i-- + ++i + ++i + i; // temp = 2 + 2 + 3 + 3
		temp = i-- + i-- + i-- + i;   // temp  = 2+ 1 + 0 + -1
		System.out.println(temp);
		System.out.println(i);

	}
}
