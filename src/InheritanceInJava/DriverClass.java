package InheritanceInJava;

public class DriverClass {
	public static void main(String[] args) {

//		Animal an = new Animal();
//		an.eat();
		
		
		Dog d = new Dog();
		d.bark();
		d.eat();
		d.show();
		
		BabyDog bd = new BabyDog();
		bd.eat();
		bd.bark();
		bd.weep();
		
		Lion l = new Lion();
		l.roar();
		l.eat();
				
	}
}
