
public class Day1 {

	public static void main(String[] args) {

		// Variable in java

//		int a;
//		a = 10;
//		System.out.println(a);
//		a = 20;
//		System.out.println(a);
		
		// Data type in java
		// Primitive 
		boolean b = true;  // 1 bit 
		char r = 'a'; 		// 2 byte
		
		byte by = 12;   // 1 byte  -128 to 127
		short s= 34;	// 2 byte	-32768 to 32767
		int i = 4;		// 4 byte	-2^31 to 2^31-1
		long l = 56;	// 8 byte	-2^63 to 2^63-1
			
		float f = 45.5f;	// 4 byte
		double o = 56.5d;	// 8 byte
			
		
	  float p = 5/2f;
	  System.out.println(p);
	  
	  
//	  Non primitive 
	  
	  String str = "Rounak";
	  int arr[] = {1,2,3,4};
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
		

	}

}
