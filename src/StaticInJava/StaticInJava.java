package StaticInJava;

public class StaticInJava {

	int rollNO;
	String name;
	static String college = "lnct";

	static {
		System.out.println("in static");
	
	}
	
	public StaticInJava(int rollNO, String name) {
		
		this.rollNO = rollNO;
		this.name = name;

	}
	public static void print() {
		
		System.out.println("in print");
	}

	public void show() {
//		college = "asfda";
		System.out.println(rollNO);
		System.out.println(name);
		System.out.println(college);
		System.out.println("***************");
	}

	public static void main(String[] args) {

//		StaticInJava obj1 = new StaticInJava(101, "rounak");
//		StaticInJava obj2 = new StaticInJava(102, "vikash");
//		StaticInJava obj3 = new StaticInJava(103, "shubham");
//		obj1.show();
//		StaticInJava.college = "RGPV";
//		obj1.show();
//		obj2.show();
//		obj3.show();
		StaticInJava.print();
	}
}
